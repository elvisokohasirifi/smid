<?php

	include 'config.php';
	require_once "libchart/libchart/classes/libchart.php";

	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  return $data;
	}

	function queryMysql($query) {    
	  	global $connection;    
	  	$result = $connection->query($query);    
	  	if (!$result) 
	  		die($connection->error);    
	  	return $result;  
	}

	function loadStudents($query=''){
		$sql = "SELECT * from customers " . $query . " order by first_name";
		$ans = '';
		$result = queryMysql($sql);
		if($result -> num_rows > 0){
	      while($row = $result -> fetch_assoc()){
	      	$ans .= '<tr>
	      			  <td>'.$row['msisdn'].'</td>
                      <td>'.$row['first_name'].'</td>
                      <td>'.$row['last_name'].'</td>
                      <td>'.$row['gender'].'</td>
                      <td>'.$row['date_of_birth'].'</td>
                      <td>'.$row['phone_no'].'</td>
                      <td>'.$row['occupation'].'</td>
                      <td>'.$row['beneficiaries'].'</td>
                      <td>'.$row['relation'].'</td>
                      <td>'.$row['phone'].'</td>
                      <td>'.$row['join_date'].'</td>          
                      <td>'.$row['planned_amount'].'</td>
                      <td>'.$row['frequency'].'</td>
                      <td>'.$row['association'].'</td>
                      <td>'.$row['social_sec_no'].'</td>
                      <td>'.$row['nature_of_business'].'</td>
                      <td>'.$row['location'].'</td>
                      <td>'.$row['zone_of_business'].'</td>
                      <td>'.$row['id_type'].'</td>
                      <td>'.$row['id_number'].'</td>
                      <td>'.$row['registration_officer'].'</td>
                      <td><a href="javascript:void(0)" onclick="update(\''.$row['customerid'].'\', \''.$row['msisdn'].'\', 
                      \''.$row['first_name'].'\', \''.$row['last_name'].'\', \''.$row['gender'].'\', \''.$row['date_of_birth'].'\', \''.$row['phone_no'].'\', \''.$row['occupation'].'\', \''.$row['beneficiaries'].'\', \''.$row['relation'].'\', \''.$row['phone'].'\', \''.$row['join_date'].'\', \''.$row['planned_amount'].'\', \''.$row['frequency'].'\', \''.$row['association'].'\',
      				\''.$row['social_sec_no'].'\', \''.$row['nature_of_business'].'\', \''.$row['location'].'\', \''.$row['zone_of_business'].'\', \''.$row['id_type'].'\', \''.$row['id_number'].'\')" style="font-family: \'Trebuchet MS\', Arial" data-toggle="modal" data-target="#student"><i style="font-size:18px" class="fa fa-pencil-square-o"> Edit </i></a> 

	      					<a href="javascript:void(0)" onclick="deleteStudent(\''.$row['msisdn'].'\')" style="font-family: \'Trebuchet MS\', Arial"><i style="font-size:18px" class="fa fa-trash-o"> Delete</i></a> </td>
	      			</tr>';
	      }
	    }
	    else{
	    	$ans = '';
	    }
	    return $ans;
	}

	

	function insertStudent($fname, $lname, $gender,$date,$msisdn,$pnum,$occupation,$nob,$location,$zone,$beneficiary, $relation, $bpnum, $amount ,$frequency,$association,$ssn,$idtype,$idnum, $ro, $joined){
		$sql1 = "SELECT customerid from customers order by customerid desc limit 1";
		$result1 = queryMysql($sql1);	
		$cont = $result1->fetch_assoc()['customerid'] + 1;
		$sql = "insert into customers(customerid, first_name, last_name, gender, date_of_birth, msisdn, phone_no, occupation, nature_of_business, location, zone_of_business, beneficiaries, relation, phone, planned_amount, frequency, association, social_sec_no, id_type, id_number, registration_officer, join_date) values('$cont', '$fname', '$lname', '$gender', '$date', '$msisdn', '$pnum', '$occupation', '$nob', '$location', '$zone', '$beneficiary', '$relation', '$bpnum', '$amount', '$frequency', '$association', '$ssn', '$idtype', '$idnum', '$ro', '$joined')";
		echo $sql;
		$result = queryMysql($sql);
		echo ($result) ? "Success" :  "Failed";
	}

	function insertPayment($customerid, $userid, $date, $amount, $type){
		$sql = "insert into contributions(smfid, date, amount, type) values('$customerid', '$date', '$amount', '$type')";
		$result = queryMysql($sql);
		echo ($result) ? "Success" :  "Failed";
	}


	function updateStudent($fname, $lname, $gender,$date,$msisdn,$pnum,$occupation,$nob,$location,$zone,$beneficiary, $relation, $bpnum, $amount ,$frequency,$association,$ssn,$idtype,$idnum, $jdate, $id){
		$sql = "update customers set  
				first_name = '$fname', 
				last_name = '$lname', 
				gender = '$gender', 
				date_of_birth = '$date', 
				phone_no = '$pnum', 
				occupation = '$occupation', 
				nature_of_business = '$nob', 
				location = '$location', 
				zone_of_business = '$zone', 
				beneficiaries = '$beneficiary', 
				relation = '$relation', 
				phone = '$bpnum', 
				planned_amount = '$amount', 
				frequency = '$frequency', 
				association = '$association', 
				social_sec_no = '$ssn', 
				id_type = '$idtype', 
				id_number = '$idnum'  
				where customerid = ". $id;
		$result = queryMysql($sql);
		echo ($result) ? "Success" :  "Failed";
	}

	function updatePayment($paymentid, $date, $amount){
		$sql = "update contributions set date = '$date', amount = '$amount' where cid = '$paymentid'";
		$result = queryMysql($sql);
		echo ($result) ? "Success" :  "Failed";
	}

	function deleteStudent($studentid){
		$sql = "delete from contributions where smfid = '".$studentid."'";
		$result = queryMysql($sql);
		$sql = "delete from customers where msisdn = '".$studentid."'";
		$result = queryMysql($sql);
		echo ($result) ? "Success" :  "Failed";
	}

	function deletePayment($studentid){
		$sql = "delete from Contributions where cid = ".$studentid;
		$result = queryMysql($sql);
		echo ($result) ? "Success" :  "Failed";
	}

	function loadStudentNames($query = ""){
		$sql = "SELECT * from customers " . $query . " order by first_name";
		$ans = '';
		$result = queryMysql($sql);
		if($result -> num_rows > 0){
	      while($row = $result -> fetch_assoc()){
	      	$ans .= '<option value="'.$row['msisdn'].'">
	      				'.$row['first_name']." ".$row['last_name'].'
	      			</option>';
	      }
	    }
	    else{
	    	$ans = '<span>No customer has been added yet</span>';
	    }
	    return $ans;
	}

	function loadStudentsRegister($query = ""){
		$sql = "SELECT msisdn, first_name, last_name from customers". $query;
		$ans = '';
		$result = queryMysql($sql);
		if($result -> num_rows > 0){
	      while($row = $result -> fetch_assoc()){
	      	$sql1 = "select sum(amount) as total from contributions where type = 'Contribution' and smfid = '".$row['msisdn']."'";
	      	//echo $row['customerid'];
			$result1 = queryMysql($sql1);	
			$cont = $result1->fetch_assoc()['total'];
			if ($cont == '')
				$cont = 0;

			$sql1 = "select sum(amount) as total from contributions where type = 'Withdrawal' and smfid = '".$row['msisdn']."'";

			$result1 = queryMysql($sql1);	
			$with = $result1->fetch_assoc()['total'];

			if($with == '')
				$with = 0;

			$ans .= '<tr>
						<td>'.$row['first_name'].'</td>
						<td>'.$row['last_name'].'</td>
						<td>'.$cont.'</td>
						<td>'.$with.'</td>
						<td>'.($cont - $with).'</td>
						<td><a href="printStatement.php?q='.$row['msisdn'].'" style="font-size:16px; font-family: \'Trebuchet MS\', Arial;">Print Statement</a></td>
					</tr>';
	      	
	      }
	    }
	    else{
	    	$ans = '';
	    }
	    return $ans;
	}

	function loadPayments($query = 'Contribution'){
		$sql = "SELECT concat(customers.first_name, ' ' ,customers.last_name) as fullname, cid, amount, date from contributions
		inner join customers on customers.msisdn = contributions.smfid where type = '$query' order by date desc";
		$ans = '';
		$result = queryMysql($sql);
		if($result -> num_rows > 0){
	      while($row = $result -> fetch_assoc()){
	      	$ans .= '<tr>
	      				<td>'.$row['fullname'].'</td>
	      				<td>'.$row['amount'].'</td>
	      				<td>'.$row['date'].'</td>
	      				<td><a href="javascript:void(0)" onclick="editPay('.$row['cid'].', \''.$row['fullname'].'\', \''.$row['amount'].'\', \''.$row['date'].'\', \''.$query.'\')" data-toggle="modal" data-target="#payment" style="font-size:16px; font-family: \'Trebuchet MS\', Arial;"><i style="font-size:18px" class="fa fa-pencil-square-o"> Edit </i></a> 

	      					<a href="javascript:void(0)" onclick="deletePay('.$row['cid'].')" style="font-size:16px; font-family: \'Trebuchet MS\', Arial;"><i style="font-size:18px" class="fa fa-trash-o"> Delete</i></a> 
                 		</td>
	      			</tr>';
	      }
	    }
	    else{
	    	$ans = '';
	    }
	    return $ans;
	}

	function loadWithdrawals($query = ''){
		$sql = "SELECT first_name, last_name, amount, date, name from contribution, customer, user where customer.customerid = contribution.customerid and user.name = contribution.enteredby and type = 'Withdrawal' order by date desc";
		$ans = '';
		$result = queryMysql($sql);
		if($result -> num_rows > 0){
	      while($row = $result -> fetch_assoc()){
	      	$ans .= '<tr>
	      				<td>'.$row['first_name'].'</td>
	      				<td>'.$row['last_name'].'</td>
	      				<td>'.$row['amount'].'</td>
	      				<td>'.$row['date'].'</td>
	      				<td><a href="javascript:void(0)" onclick="editPay('.$row['contributionid'].')" data-toggle="modal" data-target="#payment" style="font-size:16px; font-family: \'Trebuchet MS\', Arial;"><i style="font-size:18px" class="fa fa-pencil-square-o"> Edit </i></a> 

	      					<a href="javascript:void(0)" onclick="deletePay('.$row['contributionid'].')" style="font-size:16px; font-family: \'Trebuchet MS\', Arial;"><i style="font-size:18px" class="fa fa-trash-o"> Delete</i></a> 
                 		</td>
	      			</tr>';
	      }
	    }
	    else{
	    	$ans = '';
	    }
	    return $ans;
	}


	if(isset($_POST['addStudent'])){
		$fname = $lname = $gender = $date = $msisdn = $pnum = $occupation = $nob = $location = $zone = $beneficiary = $relation = $bpnum = $amount = $frequency = $association = $ssn = $idtype = $idnum = "";
		$fname = test_input($_POST['fname']);
		$lname = test_input($_POST['lname']);
		$gender = test_input($_POST['gender']);
		$date = test_input($_POST['date']);
		$msisdn = test_input($_POST['msisdn']);
		$pnum = test_input($_POST['pnum']);
		$occupation = test_input($_POST['occupation']);
		$nob = test_input($_POST['nob']);
		$location = test_input($_POST['location']);
		$zone = test_input($_POST['zone']);
		$beneficiary = test_input($_POST['beneficiary']);
		$relation = test_input($_POST['relation']);
		$bpnum = test_input($_POST['bpnum']);
		$amount = test_input($_POST['pamount']);
		$frequency = test_input($_POST['frequency']);
		$association = test_input($_POST['association']);
		$ssn = test_input($_POST['ssn']);
		$idtype = test_input($_POST['idtype']);
		$idnum = test_input($_POST['idnum']);
		insertStudent($fname, $lname, $gender,$date,$msisdn,$pnum,$occupation,$nob,$location,$zone,$beneficiary, $relation, $bpnum, $amount ,$frequency,$association,$ssn,$idtype,$idnum, $_SESSION['userid'], date('Y-m-d'));
		//echo "alert('Done')";
		header('Location: dashboard_home.php');
	}

	if(isset($_POST['updateStudent'])){
		$fname = $lname = $gender = $date = $msisdn = $pnum = $occupation = $nob = $location = $zone = $beneficiary = $relation = $bpnum = $amount = $frequency = $association = $ssn = $idtype = $idnum = $jdate = $id ="";
		$fname = test_input($_POST['fname']);
		$lname = test_input($_POST['lname']);
		$gender = test_input($_POST['gender']);
		$date = test_input($_POST['date']);
		$msisdn = test_input($_POST['msisdn']);
		$pnum = test_input($_POST['pnum']);
		$occupation = test_input($_POST['occupation']);
		$nob = test_input($_POST['nob']);
		$location = test_input($_POST['location']);
		$zone = test_input($_POST['zone']);
		$beneficiary = test_input($_POST['beneficiary']);
		$relation = test_input($_POST['relation']);
		$bpnum = test_input($_POST['bpnum']);
		$amount = test_input($_POST['pamount']);
		$frequency = test_input($_POST['frequency']);
		$association = test_input($_POST['association']);
		$ssn = test_input($_POST['ssn']);
		$idtype = test_input($_POST['idtype']);
		$idnum = test_input($_POST['idnum']);
		$jdate = test_input($_POST['jdate']);
		$id = test_input($_POST['hidden']);
		
		updateStudent($fname, $lname, $gender,$date,$msisdn,$pnum,$occupation,$nob,$location,$zone,$beneficiary, $relation, $bpnum, $amount ,$frequency,$association,$ssn,$idtype,$idnum, $jdate, $id);
		header('Location: dashboard_students.php');
	}

	if(isset($_POST['submitPayment'])){
		$id = $date = $amount = "";
		$date = test_input($_POST['datePay']);
		$id = test_input($_POST['studentid']);
		$amount = test_input($_POST['amount']);
		insertPayment($id, $_SESSION['userid'], $date, $amount, 'Contribution');
		header('Location: dashboard_home.php');
	}

	if(isset($_POST['submitWithdrawal'])){
		$id = $date = $amount = "";
		$date = test_input($_POST['datePay']);
		$id = test_input($_POST['studentid']);
		$amount = test_input($_POST['amount']);
		insertPayment($id, $_SESSION['userid'], $date, $amount, 'Withdrawal');
		header('Location: dashboard_home.php');
	}

	if(isset($_POST['updateStudent'])){
		$fname = $lname = $gender = $date = $parentname = $id = "";
		$fname = test_input($_POST['fname']);
		$lname = test_input($_POST['lname']);
		$date = test_input($_POST['date']);
		$gender = test_input($_POST['gender']);
		$parentname = test_input($_POST['parent']);
		$id = test_input($_POST['hidden']);
		updateStudent($id, $fname, $lname, $date, $gender, $parentname);
		header('Location: dashboard_students.php');
	}

	if(isset($_POST['updatePayment'])){
		$amount = test_input($_POST['amount']);
		$date = test_input($_POST['datePay']);
		$id = test_input($_POST['hidden']);
		$url = test_input($_POST['hidden']);
		updatePayment($id, $date, $amount);
		header('Location: dashboard_payment.php');
	}

	if(isset($_POST['updatePayment2'])){
		$amount = test_input($_POST['amount']);
		$date = test_input($_POST['datePay']);
		$id = test_input($_POST['hidden']);
		$url = test_input($_POST['hidden2']);
		updatePayment($id, $date, $amount);
		header('Location: dashboard_attendance.php');
	}

	if (isset($_GET['bal'])) {
		$sql1 = "select sum(amount) as total from contributions where type = 'Contribution' and smfid = '".$_GET['bal']."'";

			$result1 = queryMysql($sql1);	
			$with = $result1->fetch_assoc()['total'];

			if($with == '')
				$with = 0;
		$sql1 = "select sum(amount) as total from contributions where type = 'Withdrawal' and smfid = '".$_GET['bal']."'";

			$result1 = queryMysql($sql1);	
			$with2 = $result1->fetch_assoc()['total'];

			if($with2 == '')
				$with2 = 0;
			echo ($with - $with2);
	}

	if (isset($_GET['search'])) {
		$a = $_GET['search'];
		echo loadStudentNames(" where first_name like '%".$a."%' or last_name like '%".$a."%' ");
	}

	function drawStudentsPie(){
 
	    //new pie chart instance
	    $chart = new PieChart();
	 
	    //data set instance
	    $dataSet = new XYDataSet();
	 
	    //query all records from tde database
	    $query = "select gender, count(customerid) as number from customers group by gender";
	 
	    //execute tde query
	    $result = queryMysql( $query );
	 
	    //get number of rows returned
	    $num_results = $result->num_rows;
	 	
	    if( $num_results > 0){	    
	        while( $row = $result->fetch_assoc() ){
	            $dataSet->addPoint(new Point($row['gender'], $row['number']));
	            //echo $row['gender'] ." " .$row['number'];
	        }
	    
	        //finalize dataset
	        $chart->setDataSet($dataSet);
	 
	        //set chart title
	        $chart->setTitle("");
	        
	        //render as an image and store under "generated" folder
	        $chart->render("1.png");
	    
	        //pull tde generated chart where it was stored
	        return "<img alt='Pie chart'  src='1.png'/>";
	    
	    }else{
	        return "No customer found in the database.";
	    }
	}

	$a = date("Y");
	function drawPaymentsBarGraph($year = 2019){
		$sql = "SELECT month(contributions.date) AS month, sum(amount) as number
				FROM contributions
				where year(date) = '".$year."' and type = 'Contribution'
				GROUP By month(contributions.date)";
		$montds = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		
		$chart = new VerticalBarChart();
	    //data set instance
	    $dataSet = new XYDataSet();
	 
	    $result = queryMysql( $sql );
	 
	    //get number of rows returned
	    $num_results = $result->num_rows;
	 	
	    if( $num_results > 0){	    
	        while( $row = $result->fetch_assoc() ){
	        	$montd = $montds[$row['month']-1];
	            $dataSet->addPoint(new Point($montd, $row['number']));
	            //echo $row['gender'] ." " .$row['number'];
	        }
	    
	        //finalize dataset
	        $chart->setDataSet($dataSet);

	        $chart->getPlot()->setGraphPadding(new Padding(5, 30, 20, 50));

	 
	        //set chart title
	        $chart->setTitle("");
	        
	        //render as an image and store under "generated" folder
	        $chart->render("2.png");
	    
	        //pull tde generated chart where it was stored
	        return "<img alt='Bar chart'  src='2.png'/>";
	    
	    }else{
	        return "No payments found in the database.";
	    }
	}

	function drawPaymentsBarGraph2($year = 2019){
		$sql = "SELECT month(contributions.date) AS month, sum(amount) as number
				FROM contributions
				where year(date) = '".$year."' and type = 'Withdrawal'
				GROUP By month(contributions.date)";
		$montds = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		
		$chart = new VerticalBarChart();
	    //data set instance
	    $dataSet = new XYDataSet();
	 
	    $result = queryMysql( $sql );
	 
	    //get number of rows returned
	    $num_results = $result->num_rows;
	 	
	    if( $num_results > 0){	    
	        while( $row = $result->fetch_assoc() ){
	        	$montd = $montds[$row['month']-1];
	            $dataSet->addPoint(new Point($montd, $row['number']));
	            //echo $row['gender'] ." " .$row['number'];
	        }
	    
	        //finalize dataset
	        $chart->setDataSet($dataSet);

	        $chart->getPlot()->setGraphPadding(new Padding(5, 30, 20,50));

	 
	        //set chart title
	        $chart->setTitle("");
	        
	        //render as an image and store under "generated" folder
	        $chart->render("3.png");
	    
	        //pull tde generated chart where it was stored
	        return "<img alt='Bar chart'  src='3.png'/>";
	    
	    }else{
	        return "No payments found in the database for this year.";
	    }
	}

	function drawBarForWeek($year = 2019){
		$sql = "SELECT date, sum(amount) as number FROM contributions WHERE date >= ADDDATE(CURDATE(), INTERVAL 2-DAYOFWEEK(CURDATE()) DAY) AND date <= CURDATE() and type = 'Contribution' group by date";
		$montds = array('Mon', 'Tue', 'Wed', 'tdu', 'Fri');
		
		$chart = new HorizontalBarChart();
	    //data set instance
	    $dataSet = new XYDataSet();
	 
	    $result = queryMysql( $sql );
	 
	    //get number of rows returned
	    $num_results = $result->num_rows;
	 	
	    if( $num_results > 0){	    
	        while( $row = $result->fetch_assoc() ){
	        	$date = $row['date'];
	            $dataSet->addPoint(new Point($date, $row['number']));
	            //echo $row['gender'] ." " .$row['number'];
	        }
	    
	        //finalize dataset
	        $chart->setDataSet($dataSet);

	        $chart->getPlot()->setGraphPadding(new Padding(5, 30, 20, 80));

	 
	        //set chart title
	        $chart->setTitle("");
	        
	        //render as an image and store under "generated" folder
	        $chart->render("4.png");
	    
	        //pull tde generated chart where it was stored
	        return "<img alt='Bar chart'  src='4.png'/>";
	    
	    }else{
	        return "No payments found in the database for this week.";
	    }
	}

	function drawBarForWeek2($year = 2019){
		$sql = "SELECT date, sum(amount) as number FROM contributions WHERE date >= ADDDATE(CURDATE(), INTERVAL 2-DAYOFWEEK(CURDATE()) DAY) AND date <= CURDATE() and type = 'Withdrawal' group by date";
		$montds = array('Mon', 'Tue', 'Wed', 'tdu', 'Fri');
		
		$chart = new HorizontalBarChart();
	    //data set instance
	    $dataSet = new XYDataSet();
	 
	    $result = queryMysql( $sql );
	 
	    //get number of rows returned
	    $num_results = $result->num_rows;
	 	
	    if( $num_results > 0){	    
	        while( $row = $result->fetch_assoc() ){
	        	$date = $row['date'];
	            $dataSet->addPoint(new Point($date, $row['number']));
	            //echo $row['gender'] ." " .$row['number'];
	        }
	    
	        //finalize dataset
	        $chart->setDataSet($dataSet);

	        $chart->getPlot()->setGraphPadding(new Padding(5, 30, 20, 80));

	 
	        //set chart title
	        $chart->setTitle("");
	        
	        //render as an image and store under "generated" folder
	        $chart->render("5.png");
	    
	        //pull tde generated chart where it was stored
	        return "<img alt='Bar chart'  src='5.png'/>";
	    
	    }else{
	        return "No payments found in the database for this week.";
	    }
	}

	function getDayPayment(){
		$sql = "SELECT sum(amount) as amount from contributions where date = '" .date('Y-m-d')."' and type='Contribution'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function getDayPayment2(){
		$sql = "SELECT sum(amount) as amount from contributions where date = '" .date('Y-m-d')."' and type='Withdrawal'";
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function getWeekPayment(){
		$sql = "SELECT sum(amount) as amount FROM contributions WHERE date >= ADDDATE(CURDATE(), INTERVAL 2-DAYOFWEEK(CURDATE()) DAY) AND date <= CURDATE() and type='Contribution'";
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function getWeekPayment2(){
		$sql = "SELECT sum(amount) as amount FROM contributions WHERE date >= ADDDATE(CURDATE(), INTERVAL 2-DAYOFWEEK(CURDATE()) DAY) AND date <= CURDATE() and type='Withdrawal'";
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function getMonthPayment(){
		$sql = "SELECT sum(amount) as amount from contributions where month(date) = '" .date('m')."' and year(date) = '" . date('Y') . "' and type='Contribution'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function getMonthPayment2(){
		$sql = "SELECT sum(amount) as amount from contributions where month(date) = '" .date('m')."' and year(date) = '" . date('Y') . "' and type='Withdrawal'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function getYearPayment(){
		$sql = "SELECT sum(amount) as amount from contributions where year(date) = '" . date('Y') . "' and type='Contribution'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function getYearPayment2(){
		$sql = "SELECT sum(amount) as amount from contributions where year(date) = '" . date('Y') . "' and type='Withdrawal'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function overallcont()
	{
		$sql = "SELECT sum(amount) as amount from contributions where type = 'Contribution'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function overallwith()
	{
		$sql = "SELECT sum(amount) as amount from contributions where type = 'Withdrawal'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		return $amount;
	}

	function overalldiff()
	{
		$sql = "SELECT sum(amount) as amount from contributions where type = 'Withdrawal'" ;
		$result = queryMysql($sql);	
		$amount = $result->fetch_assoc()['amount'];
		if ($amount == '')
			$amount = 0;
		$sql = "SELECT sum(amount) as amount from contributions where type = 'Contribution'" ;
		$result = queryMysql($sql);	
		$amount2 = $result->fetch_assoc()['amount'];
		if ($amount2 == '')
			$amount2 = 0;
		return $amount2 - $amount;
	}
?>